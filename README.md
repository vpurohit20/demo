# Demo

### Introduction
 
 The Pipeline consists if the following stages

 - `before_script` : downloads python and aws cli to connect to aws
 - `build`: runs aws cli to spin up an EC2 instance and ssh into the instance and runs gitlab_install.sh file
 - `cleanup`: is a manual step to terminate the ec2 instance
 	

## Environment Variables

| Name           | Description                                  | Examples                                | Required?          |
| ----           | -----------                                  | -------                                 | ----               |
| aws_access_key | aws_access_key_id                            | JHDBCSHD8273OHBCSJHDC                   | YES                |
| aws_secret_key | aws_secret_access_key                        | 7SHIUDCkjncsiud+jhbs                    | YES                |
| aws_image      | aws instance image-id                        | ami-0fc20dd1da406780b                   | YES                |
| aws_security_group | aws instance security-group-ids          | sg-0832897198                           | YES, HTTPS HTTP AND SSH INBOUND RULE |





